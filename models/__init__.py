# -*- coding: utf-8 -*-

from . import book
from . import author
from . import category
from . import book_loan
from . import employee
from . import res_partner
