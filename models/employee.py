# -*- coding: utf-8 -*-

from odoo import fields, models, api, exceptions

class Contact(models.Model):
    _name = 'mylib.contact'

    name = fields.Char('Ten')
    phone = fields.Char('So dien thoai')
    address = fields.Char('Dia chi')

    @api.constrains('phone')
    def _phone_validate(self):
        for employee in self:
            if len(employee.phone) < 10:
                raise exceptions.ValidationError(u'So dien thoai khong hop le')

class Employee(models.Model):
    _name = 'mylib.employee'
    _description = 'My library employee'
    _inherit = 'mylib.author'
    _inherits = {'mylib.contact': 'contact'}

    name = fields.Char(related='contact.name', inherited=True)
    gender = fields.Selection(string="Gioi tinh", selection=[('nam', 'Nam'), ('nu', 'Nu'), ], required=False, )
    description = fields.Text("Ghi chu")
    contact = fields.Many2one('mylib.contact', ondelete='cascade')

