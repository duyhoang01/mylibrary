# -*- coding: utf-8 -*-

from odoo import api, fields, models

class BookLoan(models.Model):
    _name = 'mylib.book.loan'
    _rec_name = 'name'
    _description = 'Book loan'

    name = fields.Char('Ma phieu muon')
    user = fields.Many2one('res.partner', 'Nguoi muon')
    date = fields.Datetime('Ngay muon')
    date_return = fields.Datetime('Ngay tra')
    loan_details = fields.One2many(
        comodel_name='mylib.book.loan.detail',
        inverse_name='book_loan',
        string='Danh sach muon'
    )

class NewModule(models.Model):
    _name = 'mylib.book.loan.detail'
    _rec_name = 'book_loan'
    _description = 'Book loan detail'

    book_loan = fields.Many2one('mylib.book.loan', 'Phieu muon')
    book = fields.Many2one('mylib.book', 'Sach')
    amount = fields.Integer(string="So luong",
                               default=1,)
